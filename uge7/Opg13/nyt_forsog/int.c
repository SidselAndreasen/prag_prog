#include<gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


double Log(double x, void* params){
	double k = *(double *) params;
	double f = 1/sqrt(1-k*k*sin(x)*sin(x));
	return f;
}

double fun(int i, int j){
	double a=0, acc=1e-6,eps=1e-6,result,err;
	double b=j;
//	double facit[10];
//for (size_t i = 0; i < 10; i++) {
	double k = i*0.01;

  gsl_function f;
  f.function=Log;
	f.params = &k;

  int limit = 100;

	gsl_integration_workspace * workspace =
	gsl_integration_workspace_alloc(limit);
    gsl_integration_qags(&f,a,b,acc,eps,limit,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);
//facit[i] = result;
//}
	return result;
}
