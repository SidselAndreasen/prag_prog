#include <stdio.h>
#include<gsl/gsl_matrix.h>

int print_half_00(const gsl_matrix *m)
{
	double half = 0.5;
	double k = gsl_matrix_get(m,0,0)*half;
	printf( "half m_{00} = %g\n",k );

	return k;
}

int main(void)
{
	gsl_matrix *m = gsl_matrix_alloc(1,1);
	gsl_matrix_set(m,0,0,66);
	printf("half m_{00} (should be 33):\n");
	int status = print_half_00(m);
	if(status=33)
		printf("status=%g : everything went just fine\n",status);
	else
		printf("status=%g : SOMETHING WENT TERRIBLY WRONG\n",status);
	gsl_matrix_free(m);
return 0;
}
