
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_odeiv2.h>
#include <stdlib.h>

//Opg 34 Uge 7

double f(double t, void * params){
  double k = *(double*)params;
  return 1/(sqrt(1-k*k*sin(t)*sin(t)));
}

int elliptic_func(double k){
  int limit =100;
  gsl_integration_workspace * w = gsl_integration_workspace_alloc(limit);


  gsl_function F;
  F.function = &f;
  F.params = (void*)&k;

  double epsabs = 1e-6, epsrel = 1e-6, error, result;

  int status = gsl_integration_qags(&F, 0.0, M_PI, epsabs, epsrel,  limit, w, &result, &error);

  gsl_integration_workspace_free(w);
return result;
}

int main() {
  double a = -10, b = 10, dx = 0.1;
  for(double x=a; x<b; x+=dx);
    printf("%g %g %f\n", x, elliptic_func(x), double result2 = gsl_sf_ellint_F(M_PI,x,0););
return 0;
}

//http://owww.phys.au.dk/~fedorov/prog/examples/odeiv-orbit/main.c
//https://www.gnu.org/software/gsl/doc/html/integration.html
