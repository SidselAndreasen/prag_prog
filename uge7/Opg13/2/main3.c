#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_odeiv2.h>
#include <stdlib.h>
#include <gsl/gsl_sf_ellint.h>

//Opg 34 Uge 7

double f(double t, void * params){
  double k = *(double*)params;
  double f = 1/(sqrt(1-k*k*sin(t)*sin(t)));
  return f;
}

int main(void){
  int limit =100;
  gsl_integration_workspace * w = gsl_integration_workspace_alloc(limit);

  double k = 0.5;

  gsl_function F;
  F.function = &f;
  F.params = &k;

  double epsabs = 1e-6, epsrel = 1e-6, error, result;

  gsl_integration_qags(&F, 0.0, M_PI, epsabs, epsrel,  limit, w, &result, &error);
  double result2 = gsl_sf_ellint_F(M_PI,k,0);
  printf("result =%g\n",result);
  printf("result2 =%f\n",result2);

  gsl_integration_workspace_free(w);
  return 0;
}

//https://octave.sourceforge.io/gsl/function/gsl_sf_ellint_F.html
//http://owww.phys.au.dk/~fedorov/prog/examples/odeiv-orbit/main.c
//https://www.gnu.org/software/gsl/doc/html/integration.html
