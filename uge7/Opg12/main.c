#include<stdio.h>
#include<math.h>
#include<stdlib.h>

double error_func(double x);

int main(int argc, char** argv){
  double a = -5;
  if(argc>1) a=atof(argv[1]);

  double b = 5;
  if(argc>2) b=atof(argv[2]);

  double dx = 5;
  if(argc>3) dx=atof(argv[3]);

  for(double x=a;x<b;x+=dx) printf("%g %g\n",x,error_func(x));
return 0;
}
