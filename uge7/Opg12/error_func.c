#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
//hvordan får man pi med, skal den være int eller double???
int func(double x, const double u[], double * dudx, void * params){
  dudx[0] = u[1];
  dudx[0] = (2/sqrt(M_PI))*exp(-x*x);
return GSL_SUCCESS;
}

double error_func(double x){
//skal det være gsl_odeiv2
  gsl_odeiv2_system f;
  f.function = func;
  f.jacobian = NULL;
  f.dimension = 2;
  f.params = NULL;

//bedre hstart
  double acc=1e-6, eps=1e-6, hstart = copysign(0.1,x);
  gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
  		(&f,gsl_odeiv2_step_rkf45,hstart,acc,eps);

  double t = 0, u[2] = {0.0};
  gsl_odeiv2_driver_apply(driver,&t,x,u);

  gsl_odeiv2_driver_free(driver);
  	return u[0];
  }
  //Den skal integreres
