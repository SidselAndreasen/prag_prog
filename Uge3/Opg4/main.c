#include <stdio.h>
#include "komplex.h"
#define TINY 1e-6
int main() {
	komplex a = {1,2}, b = {3,4};

	printf("testing komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should = ", R);
	komplex_print("a+b actually = ", r);

	printf("testing komplex_sub..\n");
	komplex sr = komplex_sub(a,b);
	komplex_print("a-b =",sr);

	printf("testing komplex_mul..\n");
	komplex mr = komplex_mul(a,b);
	komplex_print("a*b =",mr);

	double na = 1, nb = 3;
	printf("testing komplex_new..\n");
	komplex n  = komplex_new(na, nb);
	komplex_print("na+i*nb= ",n);

	double x = 1, y = 3;
	komplex z = {0,0};
	printf("testing komplex_set..\n");
	komplex_set(&z,x,y);
	komplex_print("z=",*(&z));



/*the following is optional */
/*
	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'add' passes :) \n");
	else
		printf("test 'add' failes: debud me, please..\n");
*/
}


