#include "nvector.h"
#include <stdio.h>
#include <stdlib.h>
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL)
		printf("test failed\n");
	else
		printf("test passed\n");


	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if ((vi==value))
		printf("test passed\n");
	else
		printf("test failed\n");



	printf("nmain: testing nvector_dot_product ... \n");
	nvector *dv = nvector_alloc(n);
	nvector *du = nvector_alloc(n);
	double k = 3;
	for(int j=0; j<n; j++){nvector_set(dv,j,k);}
	for(int l=0; l<n; l++){nvector_set(du,l,k);}
		double dot = nvector_dot_product(dv,du);
	double result = 45;
	if(dot==result)
		printf("result true\n");
	else
		printf("fejjjl\n");

	nvector_free(v);
	nvector_free(dv);
	nvector_free(du);

	return 0;
}

