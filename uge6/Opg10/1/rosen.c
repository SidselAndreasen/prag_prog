#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<assert.h>

int rosen_eq
(const gsl_vector* v, void* params, gsl_vector* f){
	double x=gsl_vector_get(v,0);
  double y=gsl_vector_get(v,1);
  double f0=2*(1-x)*(-1)+2*100*(y-x*x)*(-2*x);
  double f1=2*100*(y-x*x);
	gsl_vector_set(f,0,f0);
  gsl_vector_set(f,1,f1);
return GSL_SUCCESS;
}

int main(){
  int dim = 2;

const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_hybrid;
gsl_multiroot_fsolver* S = gsl_multiroot_fsolver_alloc(T,dim);

gsl_multiroot_function F;
F.f=rosen_eq;
F.n=dim;
F.params=NULL;

gsl_vector* start = gsl_vector_alloc(dim);
gsl_vector_set(start,0,2);
gsl_vector_set(start,1,3);
gsl_multiroot_fsolver_set(S,&F,start);

int flag;
do{
gsl_multiroot_fsolver_iterate(S);
flag = gsl_multiroot_test_residual(S->f,1e-6);
}while(flag==GSL_CONTINUE);

double x = gsl_vector_get(S->x,0);
double y = gsl_vector_get(S->x,1);
gsl_vector_free(start);
gsl_multiroot_fsolver_free(S);
printf("x=%g, y=%g\n",x,y);
return 0;
}
