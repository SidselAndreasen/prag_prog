#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

double rosen_eq(const gsl_vector* v, void* params){
	double x=gsl_vector_get(v,0);
  double y=gsl_vector_get(v,1);
return (1-x)*(1-x)+100*(y-x*x)*(y-x*x);
}

int main(){
  const int dim = 2;
  gsl_multimin_fminimizer *M
    = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,dim);

  gsl_multimin_function F;
  F.f = rosen_eq;
  F.n = dim;
  F.params = NULL;

  gsl_vector* start = gsl_vector_alloc(dim);
  gsl_vector_set(start,0,10);
  gsl_vector_set(start,1,-11);

  gsl_vector* step = gsl_vector_alloc(dim);
  gsl_vector_set(step,0,0.1);
  gsl_vector_set(step,1,0.1);

gsl_multimin_fminimizer_set(M,&F,start,step);

int iter=0;
do{
iter++;
gsl_multimin_fminimizer_iterate(M);
double x=gsl_vector_get(M->x,0);
double y=gsl_vector_get(M->x,1);
fprintf(stderr,"iter=%i x=%g y=%g\n",iter,x,y);
int status = gsl_multimin_test_size(M->size,1e-2);
if(status==GSL_SUCCESS)break;
}while(1);

double x=gsl_vector_get(M->x,0);
double y=gsl_vector_get(M->x,1);
//double f(double x y){ return (1-x)*(1-x)+100*(y-x*x)*(y-x*x));}

//for(double x=e[0];x<e[n-1]+0.1;x+=0.05)printf("%g %g\n",x,f(x));

fprintf(stderr,"x=%g y=%g\n",x,y);


gsl_multimin_fminimizer_free(M);
gsl_vector_free(step);

return 0;
}
