#include <stdio.h>
#include <limits.h>
#include <float.h>
int fkt1(){

double de=1;
while(1+de!=1){de/=2;} de*=2;
	printf("double epsilon =%g\n",de);

float fe=1.0f;
while(1+fe!=1){fe/=2;} fe*=2;
        printf("float epsilon =%g\n",fe);

long double le=1.0L;
while(1+le!=1){le/=2;} le*=2;
        printf("double epsilon =%Lg\n",le);

double dee;
for(dee=1; 1+dee!=1; dee/=2){} dee*=2;
	printf("double epsilon =%g\n",dee);

long double lee;
for(lee=1; 1+lee!=1; lee/=2){} lee*=2;
        printf("long double epsilon =%Lg\n",lee);

float fee;
for(fee=1; 1+fee!=1; fee/=2){} fee*=2;
        printf("float epsilon =%g\n",fee);

double deee=1;
do
{deee/=2;}
while(1+deee!=1);
        printf("double epsilon=%g\n",deee*=2);

long double leee=1;
do
{leee/=2;}
while(1+leee!=1);
        printf("long double spsilon=%Lg\n",leee*=2);

float feee=1;
do
{feee/=2;}
while(1+feee!=1);
        printf("float epsilon=%g\n",feee*=2);

return 0;
}
