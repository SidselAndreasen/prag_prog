#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
// løs ligning system Ax=b
int main() {
//Lav matrix og allocer, husk free!!!
gsl_matrix* M = gsl_matrix_alloc(3,3);
gsl_matrix_set(M,0,0,6.13);
gsl_matrix_set(M,0,1,-2.90);
gsl_matrix_set(M,0,2,5.86);
gsl_matrix_set(M,1,0,8.08);
gsl_matrix_set(M,1,1,-6.31);
gsl_matrix_set(M,1,2,-3.89);
gsl_matrix_set(M,2,0,-4.36);
gsl_matrix_set(M,2,1,1.00);
gsl_matrix_set(M,2,2,0.19);
//Den sletter inholdet af A, så vi laver en kopi
gsl_matrix* M2 = gsl_matrix_alloc(3,3);

gsl_matrix_memcpy(M2, M);

gsl_vector* b = gsl_vector_alloc(3);

gsl_vector_set(b,0,6.23);
gsl_vector_set(b,1,5.37);
gsl_vector_set(b,2,2.29);

gsl_vector* x = gsl_vector_alloc(3);

gsl_linalg_HH_solve (M, b, x);
  printf("x=\n");
  gsl_vector_fprintf(stdout,x,"%g");
//test om A*x=b her hedder b y
  gsl_vector* y=gsl_vector_calloc(3);
    gsl_blas_dgemv(CblasNoTrans,1,M2,x,0,y);
      printf("b=\n");
    gsl_vector_fprintf(stdout,y,"%g");

gsl_vector_free(b);
gsl_matrix_free(M);
gsl_vector_free(x);
gsl_matrix_free(M2);


}
