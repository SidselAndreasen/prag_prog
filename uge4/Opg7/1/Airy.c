#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <gsl/gsl_sf_airy.h>
#include <gsl/gsl_sf.h>
int main() {
	for(double x=-10; x<2; x+=0.1)
		printf("%g %g %g\n",x, gsl_sf_airy_Ai(x,GSL_PREC_DOUBLE), gsl_sf_airy_Bi(x,GSL_PREC_APPROX));





//	int gsl_sf_airy_Ai_e(double x, gsl_mode_t $GSL_PREC_DOUBLE, gsl_sf_result*result);
//		printf("%g",gsl_sf_airy_Ai);
//        double gsl_sf_airy_Bi(double x, gsl_mode_t $GSL_PREC_APPROX);
//        int gsl_sf_airy_Bi_e(double x, gsl_mode_t $GSL_PREC_APPROX, gsl_sf_result*result);
return 0;
}
