#include <stdio.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_matrix.h>
#include<getopt.h>
#include<math.h>
#include<gsl/gsl_errno.h>
//løs y'(x)=y(x)*(1-y(x)) fra x=0 til x=3 med y(0)=0.5
int func(double t, const double y[], double dydx[], void * params){
  //y(0)=0.5
  dydx[0]=y[1];
  dydx[0]=y[0]*(1-y[0]);
  return GSL_SUCCESS;
}
double my_ode (double x){
 gsl_odeiv2_system sys;
 sys.function=func;
 sys.jacobian=NULL;
 sys.dimension=2;
 sys.params=NULL;

  double hstart=copysign(0.1,x);
  double acc=1e-6;
  double eps=1e-6;
  gsl_odeiv2_driver* driver =
    gsl_odeiv2_driver_alloc_y_new
    (&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

//y(t)=y(0)=0.5
  double t=0;
  double y[2]={0.5};
  gsl_odeiv2_driver_apply(driver,&t,x,y);
//https://www.gnu.org/software/gsl/doc/html/ode-initval.html
	gsl_odeiv2_driver_free(driver);
  return y[0];
}

int main(){

	for(double x=0;x<3;x+=0.1)
		printf("%g %g\n",x,my_ode(x));
    printf("\n");
  	printf("\n");
  	for(double x=0;x<3;x+=0.1)
  		printf("%g %g\n",x,1/(1+exp(-x)));
return 0;
}
