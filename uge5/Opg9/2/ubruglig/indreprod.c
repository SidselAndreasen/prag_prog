#include<gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>
#include <math.h>

double ind(double x, void* params){
  	double alpha = *(double*)params;
  	return exp(-alpha*pow(x,2));
  }

  double indreprod(double alpha){


  	gsl_function f;
  	f.function = ind;
    f.params = (void*)&alpha;

  	int limit = 100;
  	double acc=1e-6,eps=1e-6,result,err;
  	gsl_integration_workspace * workspace =
  	gsl_integration_workspace_alloc(limit);
    gsl_integration_qagi(&f,acc,eps,limit,workspace,&result,&err);
  	gsl_integration_workspace_free(workspace);
  	return result;
  }
