#include<gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>
#include <math.h>


double Log(double x, void* params){
	return log(x)/sqrt(x);
}

double fun(){

  gsl_function f;
  f.function=Log;

  int limit = 100;
	double a=0, b=1, acc=1e-6,eps=1e-6,result,err;
	gsl_integration_workspace * workspace =
	gsl_integration_workspace_alloc(limit);
    gsl_integration_qags(&f,a,b,acc,eps,limit,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);
	return result;
}
